import React from 'react';
import { Button, Form, FormGroup, Label, Input, Container, Row} from 'reactstrap';




export default class Contacto extends React.Component {

     constructor(props) {
         super(props);
         this.state = {
            texto: ""
         }
         this.pulsar = this.pulsar.bind(this);
         this.cambioInput = this.cambioInput.bind(this);
     }

     cambioInput(event) {
        const value = event.target.type === 'checkbox' ? event.target.checked : event.target.value;
        console.log(value)
        const name = event.target.name;
        this.setState({ [name]: value });
    }

    pulsar() {
        this.setState({
            texto: this.state.texto
        })
    }

    render() {
        console.log(this.state);
        return (
            <div>
            <Container> 
                <Form>
                    <FormGroup>
                        <Label for="exampleText">Text Area</Label>
                        <Input type="textarea" name="texto" id="exampleText" onChange={this.cambioInput} value={this.state.texto}/>
                    </FormGroup>
                    <FormGroup>
                        <Button onClick={this.pulsar} >Enviar</Button>
                    </FormGroup>
                </Form>
                <div>
                    {/* <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d925424.440290043!2d-83.6647260607519!3d39.97718250091391!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x88388ef561620cb7%3A0x3251fcb3c752b49a!2sRogue+Fitness!5e0!3m2!1sen!2ses!4v1530614871845" width="100%" height="100%" frameBorder="0" allowFullScreen></iframe> */}

                        <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d23943.72944496081!2d2.137696799321589!3d41.39654081342877!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1stomo+2!5e0!3m2!1ses!2ses!4v1557834407417!5m2!1ses!2ses" width="100%" height="100%" frameBorder="0" allowFullScreen></iframe>
                    </div>
            </Container>
            </div>
        );
    }

}
