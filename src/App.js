import React from 'react';


import Contacto from './componentes/Contacto';



export default class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      encendido: this.props.estadoInicial,
    }
    this.pulsar = this.pulsar.bind(this);
  }

  pulsar() {
    this.setState({
      encendido: !this.state.encendido
    })
  }

  render() {

    return (
      <div>
        <Contacto/>
      </div>
    );
  }

}

